package cn.chongho.inf.flink.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author feihu.wang
 * @since 2022-02-14
 */
@Data
@Table(name = "users")
public class AdminUser {
    @Id
    private Integer id;

    private Integer tenantid;

    private String account;

    private String name;

    private String psw;

    private String email;

    private Integer creator;

    @Transient
    private String createUser;

    private Date createtime;

    private Integer flag;

    private Date logintime;

    private Integer updateuser;

    private Date updatetime;


    public static AdminUser createByUserInfo(String account, String userName){
        AdminUser adminUser = new AdminUser();
        adminUser.setAccount(account);
        adminUser.setName(userName);
        adminUser.setPsw("");
        adminUser.setEmail("");
        adminUser.setCreator(-1);
        adminUser.setUpdateuser(-1);
        adminUser.setFlag(1);
        adminUser.setCreatetime(new Date());
        adminUser.setUpdatetime(new Date());
        adminUser.setLogintime(new Date());
        adminUser.setTenantid(0);
        return adminUser;
    }
}
